package error_handling

class InvalidPieceException(
    override val message: String? = null
): Exception()