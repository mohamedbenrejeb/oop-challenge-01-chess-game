package pieces

class King(
    override val file: Char,
    override val rank: Char,
): Piece {
    override fun move() {

    }

    override fun getPossibleMoves() {

    }

    override fun decode(): String {
        return "k$file$rank"
    }
}