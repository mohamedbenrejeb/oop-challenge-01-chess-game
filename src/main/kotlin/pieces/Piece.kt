package pieces

import error_handling.InvalidPieceException

interface Piece {
    val file: Char
    val rank: Char

    fun move()
    fun getPossibleMoves()

    fun decode(): String

    companion object {
        // Factory Method
        fun from(decodedPiece: String): Piece {
            return when(decodedPiece.first()) {
                'q' -> Queen(file = decodedPiece[1], rank = decodedPiece[2])
                'k' -> King(file = decodedPiece[1], rank = decodedPiece[2])
                else -> throw InvalidPieceException(message = "There is piece with this code: ${decodedPiece.first()}")
            }
        }
    }
}