package pieces

class Queen(
    override val file: Char,
    override val rank: Char,
) : Piece {
    override fun move() {}

    override fun getPossibleMoves() {}

    override fun decode(): String {
        // Example
        // file: 'd'
        // rank: '4'
        return "q$file$rank" // "qd4"
    }
}