import pieces.Piece
import pieces.Queen

fun main(args: Array<String>) {
    val piece: Piece = Queen(file = 'd', rank = '4')
    val decodedPiece = piece.decode()
    println(decodedPiece)
}